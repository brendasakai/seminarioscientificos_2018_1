package br.com.mauda.seminario.cientificos.bc;

import java.util.Date;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dao.SeminarioDAO;
import br.com.mauda.seminario.cientificos.dto.SeminarioDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.AreaCientifica;
import br.com.mauda.seminario.cientificos.model.Professor;
import br.com.mauda.seminario.cientificos.model.Seminario;

public class SeminarioBC extends PatternCrudBC<Seminario, SeminarioDTO, SeminarioDAO> {

    @Inject
    private ProfessorBC professorBC;
    @Inject
    private AreaCientificaBC areaCientificaBC;

    @Override
    protected void validateForDataModification(Seminario object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (object.getData() == null) {
            throw new SeminariosCientificosException("ER0070");
        }

        if (object.getData().before(new Date())) {
            throw new SeminariosCientificosException("ER0070");
        }

        if (StringUtils.isBlank(object.getDescricao())) {
            throw new SeminariosCientificosException("ER0071");
        }

        if (object.getDescricao().length() > 200) {
            throw new SeminariosCientificosException("ER0071");
        }

        if (StringUtils.isBlank(object.getTitulo()) || object.getTitulo().length() > 50 || object.getTitulo().equals("     ")) {
            throw new SeminariosCientificosException("ER0072");
        }

        if (object.getMesaRedonda() == null) {
            throw new SeminariosCientificosException("ER0073");
        }

        if (object.getQtdInscricoes() == null) {
            throw new SeminariosCientificosException("ER0074");
        }

        if (object.getQtdInscricoes() <= 0) {
            throw new SeminariosCientificosException("ER0074");
        }

        if (object.getProfessores().isEmpty()) {
            throw new SeminariosCientificosException("ER0075");
        }

        if (object.getAreasCientificas().isEmpty()) {
            throw new SeminariosCientificosException("ER0076");
        }

        for (Professor professor : object.getProfessores()) {
            this.professorBC.validateForDataModification(professor);
        }

        for (AreaCientifica areaCientifica : object.getAreasCientificas()) {
            this.areaCientificaBC.validateForDataModification(areaCientifica);
        }
    }

    @Override
    protected boolean validateForFindData(SeminarioDTO object) {
        return object != null && (object.getId() != null ||
            object.getData() != null ||
            object.getMesaRedonda() != null ||
            StringUtils.isNotBlank(object.getNomeAreaCientifica()) ||
            StringUtils.isNotBlank(object.getNomeProfessor()) ||
            StringUtils.isNotBlank(object.getTitulo()) ||
            StringUtils.isNotBlank(object.getDescricao()));
    }
}