package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.EstudanteDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Estudante;

public class EstudanteDAO extends PatternCrudDAO<Estudante, EstudanteDTO> {

    private static final long serialVersionUID = 1L;

    public EstudanteDAO() {
        super(Estudante.class);
    }

    @Override
    public void inicializaLazyObjects(Estudante object) {
        if (object != null) {
            Hibernate.initialize(object.getInscricoes());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<Estudante> findByFilter(EstudanteDTO filter) {
        try {
            Criteria c = this.session.createCriteria(this.entityClassName);
            c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

            if (filter.getId() != null) {
                c.add(Restrictions.eq("id", filter.getId()));
            }

            if (StringUtils.isNotBlank(filter.getEmail())) {
                c.add(Restrictions.like("email", "%" + filter.getEmail() + "%"));
            }
            if (StringUtils.isNotBlank(filter.getTelefone())) {
                c.add(Restrictions.like("telefone", "%" + filter.getTelefone() + "%"));
            }

            if (StringUtils.isNotBlank(filter.getNome())) {
                c.add(Restrictions.like("nome", "%" + filter.getNome() + "%"));
            }

            if (StringUtils.isNotBlank(filter.getNomeInstituicao()) || StringUtils.isNotBlank(filter.getCidade())
                || StringUtils.isNotBlank(filter.getEstado()) || StringUtils.isNotBlank(filter.getPais())) {

                c.createAlias("instituicao", "instituicao");

                if (StringUtils.isNotBlank(filter.getNomeInstituicao())) {
                    c.add(Restrictions.ilike("instituicao.nome", "%" + filter.getNomeInstituicao() + "%"));
                }
                if (StringUtils.isNotBlank(filter.getCidade())) {
                    c.add(Restrictions.ilike("instituicao.cidade", "%" + filter.getCidade() + "%"));
                }
                if (StringUtils.isNotBlank(filter.getEstado())) {
                    c.add(Restrictions.ilike("instituicao.estado", "%" + filter.getEstado() + "%"));
                }
                if (StringUtils.isNotBlank(filter.getPais())) {
                    c.add(Restrictions.ilike("instituicao.pais", "%" + filter.getPais() + "%"));
                }
            }

            Collection<Estudante> collection = c.list();
            for (Estudante object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;

        } catch (Exception e) {
            throw new SeminariosCientificosException("ER0003");
        }
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
