package br.com.mauda.seminario.cientificos.dao;

import java.io.Serializable;
import java.util.Collection;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import br.com.mauda.seminario.cientificos.dao.util.OperacaoTransacional;
import br.com.mauda.seminario.cientificos.dao.util.PossuiSession;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.IdentifierInterface;

public abstract class PatternCrudDAO<T extends IdentifierInterface, DTO> implements Serializable, PossuiSession {

    private static final long serialVersionUID = 3723942253378506052L;
    protected String entityClassName;
    protected static Logger LOGGER;

    @Inject
    public Session session;

    public PatternCrudDAO(Class<T> entityClass) {
        PatternCrudDAO.LOGGER = LogManager.getLogger(entityClass);
        this.entityClassName = entityClass.getName();
    }

    @Override
    public Session getSession() {
        return this.session;
    }

    ////////////////////////////////////////////////////////////
    // METODOS AUXILIARES
    ////////////////////////////////////////////////////////////
    /**
     * Metodo auxiliar para inicializar um objeto que eh lazy
     *
     * @param collection
     */
    public abstract void inicializaLazyObjects(T object);

    ////////////////////////////////////////////////////////////
    // METODOS DML DE RECUPERACAO DE INFORMACAO
    ////////////////////////////////////////////////////////////

    /**
     * Metodo que realiza uma busca pelo id na tabela da entidade T
     *
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked")
    public T findById(Long id) {
        try {
            Query byIdQuery = this.session.createQuery("FROM " + this.entityClassName + " as c WHERE c.id = :id");
            byIdQuery.setParameter("id", id);
            T object = (T) byIdQuery.uniqueResult();
            this.inicializaLazyObjects(object);
            return object;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }

    /**
     * Utilizado para buscas com o filtro da entidade, onde este contera as informacoes relacionadas com a filtragem de dados
     *
     * @param filter
     * @return
     */
    public abstract Collection<T> findByFilter(DTO filter);

    /**
     * Metodo que realiza a busca de todas as entidades da tabela da entidade T
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public Collection<T> findAll() {
        try {
            Query listQuery = this.session.createQuery("FROM " + this.entityClassName + " as c");
            Collection<T> collection = listQuery.list();
            for (T object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;
        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }

    /////////////////////////////////////////
    // METODOS DML COM ALTERACAO NA BASE
    /////////////////////////////////////////

    /**
     * Metodo que realiza um insert na tabela da entidade T
     *
     * @param obj
     * @return
     */
    @OperacaoTransacional
    public void insert(T obj) {
        this.session.persist(obj);
    }

    /**
     * Metodo que realiza um update na tabela da entidade T
     *
     * @param obj
     * @return
     */
    @OperacaoTransacional
    public void update(T obj) {
        this.session.merge(obj);
        PatternCrudDAO.LOGGER.debug("Linha: " + obj + ", foi atualizada. ");
    }

    /**
     * Metodo que realiza um delete na tabela da entidade T
     *
     * @param obj
     * @return
     */

    @OperacaoTransacional
    public void delete(T obj) {
        this.session.merge(obj);
        this.session.delete(obj);
    }

}