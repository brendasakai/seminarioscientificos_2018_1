package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_SEMINARIO")
public class Seminario implements IdentifierInterface, Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    private String titulo;
    private String descricao;

    @Column(name = "MESA_REDONDA")
    private Boolean mesaRedonda;

    @Column(name = "QTD_INSCRICOES")
    private Integer qtdInscricoes;

    @Temporal(TemporalType.DATE)
    private Date data;

    @ManyToMany(targetEntity = Professor.class)
    @JoinTable(name = "TB_PROFESSOR_SEMINARIO",
        joinColumns = {@JoinColumn(name = "ID_SEMINARIO") },
        inverseJoinColumns = {@JoinColumn(name = "ID_PROFESSOR") })
    private List<Professor> professores = new ArrayList<>();

    @ManyToMany(targetEntity = AreaCientifica.class)
    @JoinTable(name = "TB_SEMINARIO_AREA_CIENTIFICA",
        joinColumns = {@JoinColumn(name = "ID_SEMINARIO") },
        inverseJoinColumns = {@JoinColumn(name = "ID_AREA_CIENTIFICA") })
    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    @OneToMany(targetEntity = Inscricao.class, mappedBy = "seminario", cascade = {CascadeType.PERSIST, CascadeType.REMOVE })
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Seminario() {
        // TODO Auto-generated constructor stub
    }

    public Seminario(AreaCientifica areaCientifica, Professor professor, int qtdInscricoes) {

        this.areasCientificas.add(areaCientifica);
        this.professores.add(professor);
        professor.getSeminarios().add(this);
        this.qtdInscricoes = qtdInscricoes;

        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }

    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public void setAreasCientificas(List<AreaCientifica> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setInscricoes(List<Inscricao> inscricoes) {
        this.inscricoes = inscricoes;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Seminario other = (Seminario) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}