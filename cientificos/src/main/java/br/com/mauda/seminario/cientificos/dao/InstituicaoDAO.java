package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.InstituicaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Instituicao;

public class InstituicaoDAO extends PatternCrudDAO<Instituicao, InstituicaoDTO> {

    private static final long serialVersionUID = 1L;

    public InstituicaoDAO() {
        super(Instituicao.class);
    }

    @Override
    public void inicializaLazyObjects(Instituicao object) {

    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<Instituicao> findByFilter(InstituicaoDTO filter) {
        try {
            Criteria c = this.session.createCriteria(this.entityClassName);
            c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

            if (filter.getId() != null) {
                c.add(Restrictions.eq("id", filter.getId()));
            }
            if (StringUtils.isNotBlank(filter.getCidade())) {
                c.add(Restrictions.ilike("cidade", "%" + filter.getCidade() + "%"));
            }
            if (StringUtils.isNotBlank(filter.getNome())) {
                c.add(Restrictions.ilike("nome", "%" + filter.getNome() + "%"));
            }
            if (StringUtils.isNotBlank(filter.getEstado())) {
                c.add(Restrictions.ilike("estado", "%" + filter.getEstado() + "%"));
            }
            if (StringUtils.isNotBlank(filter.getPais())) {
                c.add(Restrictions.ilike("pais", "%" + filter.getPais() + "%"));
            }
            if (StringUtils.isNotBlank(filter.getSigla())) {
                c.add(Restrictions.ilike("sigla", "%" + filter.getSigla() + "%"));
            }

            Collection<Instituicao> collection = c.list();
            for (Instituicao object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }

    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
