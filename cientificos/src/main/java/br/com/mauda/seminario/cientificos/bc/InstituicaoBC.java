package br.com.mauda.seminario.cientificos.bc;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dao.InstituicaoDAO;
import br.com.mauda.seminario.cientificos.dto.InstituicaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Instituicao;

public class InstituicaoBC extends PatternCrudBC<Instituicao, InstituicaoDTO, InstituicaoDAO> {

    @Override
    protected void validateForDataModification(Instituicao object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (StringUtils.isBlank(object.getCidade())) {
            throw new SeminariosCientificosException("ER0050");
        }

        if (StringUtils.isBlank(object.getEstado())) {
            throw new SeminariosCientificosException("ER0051");
        }

        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0052");
        }

        if (StringUtils.isBlank(object.getPais())) {
            throw new SeminariosCientificosException("ER0053");
        }

        if (StringUtils.isBlank(object.getSigla())) {
            throw new SeminariosCientificosException("ER0054");
        }

        if (object.getCidade().length() > 50) {
            throw new SeminariosCientificosException("ER0050");
        }

        if (object.getEstado().length() > 50) {
            throw new SeminariosCientificosException("ER0051");
        }

        if (object.getNome().length() > 100) {
            throw new SeminariosCientificosException("ER0052");
        }

        if (object.getPais().length() > 10) {
            throw new SeminariosCientificosException("ER0053");
        }

        if (object.getSigla().length() > 10) {
            throw new SeminariosCientificosException("ER0054");
        }

    }

    @Override
    protected boolean validateForFindData(InstituicaoDTO object) {
        // TODO Auto-generated method stub
        return object != null && (object.getId() != null ||
            StringUtils.isNotBlank(object.getNome()) ||
            StringUtils.isNotBlank(object.getCidade()) ||
            StringUtils.isNotBlank(object.getEstado()) ||
            StringUtils.isNotBlank(object.getPais()) ||
            StringUtils.isNotBlank(object.getSigla()));
    }
}