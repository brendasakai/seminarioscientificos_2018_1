package br.com.mauda.seminario.cientificos.dto;

import java.util.Date;

public class SeminarioDTO {

    private Long id;
    private Date data;
    private String descricao;
    private Boolean mesaRedonda;
    private String nomeAreaCientifica;
    private Long idAreaCientifica;
    private String nomeProfessor;
    private String titulo;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public String getNomeAreaCientifica() {
        return this.nomeAreaCientifica;
    }

    public void setNomeAreaCientifica(String nomeAreaCientifica) {
        this.nomeAreaCientifica = nomeAreaCientifica;
    }

    public String getNomeProfessor() {
        return this.nomeProfessor;
    }

    public void setNomeProfessor(String nomeProfessor) {
        this.nomeProfessor = nomeProfessor;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Long getIdAreaCientifica() {
        return this.idAreaCientifica;
    }

    public void setIdAreaCientifica(Long idAreaCientifica) {
        this.idAreaCientifica = idAreaCientifica;
    }

}
