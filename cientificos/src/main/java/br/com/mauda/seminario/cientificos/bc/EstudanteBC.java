package br.com.mauda.seminario.cientificos.bc;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dao.EstudanteDAO;
import br.com.mauda.seminario.cientificos.dto.EstudanteDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Estudante;
import br.com.mauda.seminario.cientificos.util.EmailUtils;

public class EstudanteBC extends PatternCrudBC<Estudante, EstudanteDTO, EstudanteDAO> {

    @Inject
    private InstituicaoBC instituicaoBC;

    @Override
    protected void validateForDataModification(Estudante object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (StringUtils.isBlank(object.getEmail())) {
            throw new SeminariosCientificosException("ER0030");
        }

        if (EmailUtils.EMAIL_PATTERN.matches(object.getEmail())) {
            throw new SeminariosCientificosException("ER0030");
        }

        if (object.getEmail().length() > 50) {
            throw new SeminariosCientificosException("ER0030");
        }

        if (!object.getEmail().contains("@")) {
            throw new SeminariosCientificosException("ER0030");
        }

        if (!object.getEmail().contains(".")) {
            throw new SeminariosCientificosException("ER0030");
        }

        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0031");
        }

        if (object.getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0031");
        }

        if (StringUtils.isBlank(object.getTelefone())) {
            throw new SeminariosCientificosException("ER0032");
        }

        if (object.getTelefone().length() > 15) {
            throw new SeminariosCientificosException("ER0032");
        }

        this.instituicaoBC.validateForDataModification(object.getInstituicao());

    }

    @Override
    protected boolean validateForFindData(EstudanteDTO object) {
        // TODO Auto-generated method stub
        return object != null && (object.getId() != null ||
            object.getIdInstituicao() != null ||
            StringUtils.isNotBlank(object.getNome()) ||
            StringUtils.isNotBlank(object.getCidade()) ||
            StringUtils.isNotBlank(object.getEstado()) ||
            StringUtils.isNotBlank(object.getEmail()) ||
            StringUtils.isNotBlank(object.getTelefone()) ||
            StringUtils.isNotBlank(object.getNomeInstituicao()) ||
            StringUtils.isNotBlank(object.getCidadeInstituicao()) ||
            StringUtils.isNotBlank(object.getEstadoInstituicao()) ||
            StringUtils.isNotBlank(object.getPaisInstituicao()) ||
            StringUtils.isNotBlank(object.getPais()));
    }
}