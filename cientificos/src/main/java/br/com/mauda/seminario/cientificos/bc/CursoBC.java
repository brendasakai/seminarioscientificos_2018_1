package br.com.mauda.seminario.cientificos.bc;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dao.CursoDAO;
import br.com.mauda.seminario.cientificos.dto.CursoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Curso;

public class CursoBC extends PatternCrudBC<Curso, CursoDTO, CursoDAO> {

    @Inject
    private AreaCientificaBC areaCientificaBC;

    @Override
    protected void validateForDataModification(Curso object) {
        // TODO Auto-generated method stub

        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0020");
        }

        if (object.getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0020");
        }

        this.areaCientificaBC.validateForDataModification(object.getAreaCientifica());

    }

    @Override
    protected boolean validateForFindData(CursoDTO object) {
        // TODO Auto-generated method stub
        return object != null && (object.getId() != null ||
            StringUtils.isNotBlank(object.getNome()) ||
            StringUtils.isNotBlank(object.getNomeAreaCientifica()));
    }
}