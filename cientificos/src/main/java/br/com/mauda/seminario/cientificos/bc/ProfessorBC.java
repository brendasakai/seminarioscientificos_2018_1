package br.com.mauda.seminario.cientificos.bc;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dao.ProfessorDAO;
import br.com.mauda.seminario.cientificos.dto.ProfessorDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Professor;
import br.com.mauda.seminario.cientificos.util.EmailUtils;

public class ProfessorBC extends PatternCrudBC<Professor, ProfessorDTO, ProfessorDAO> {

    @Inject
    private InstituicaoBC instituicaoBC;

    @Override
    protected void validateForDataModification(Professor object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (StringUtils.isBlank(object.getEmail())) {
            throw new SeminariosCientificosException("ER0060");
        }

        if (EmailUtils.EMAIL_PATTERN.matches(object.getEmail())) {
            throw new SeminariosCientificosException("ER0060");
        }

        if (object.getEmail().length() > 50) {
            throw new SeminariosCientificosException("ER0060");
        }

        if (object.getEmail().contains("@") == false) {
            throw new SeminariosCientificosException("ER0060");
        }

        if (object.getEmail().contains(".") == false) {
            throw new SeminariosCientificosException("ER0060");
        }

        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0061");
        }

        if (object.getNome().length() >= 50) {
            throw new SeminariosCientificosException("ER0061");
        }

        if (StringUtils.isBlank(object.getTelefone())) {
            throw new SeminariosCientificosException("ER0062");
        }

        if (object.getTelefone().length() > 15) {
            throw new SeminariosCientificosException("ER0062");
        }

        if (object.getSalario() == null) {
            throw new SeminariosCientificosException("ER0063");
        }

        if (object.getSalario() < 0) {
            throw new SeminariosCientificosException("ER0063");
        }

        if (object.getSalario() == 0) {
            throw new SeminariosCientificosException("ER0063");
        }

        this.instituicaoBC.validateForDataModification(object.getInstituicao());

    }

    @Override
    protected boolean validateForFindData(ProfessorDTO object) {
        // TODO Auto-generated method stub
        return object != null && (object.getId() != null ||
            object.getSalario() != null ||
            StringUtils.isNotBlank(object.getNome()) ||
            StringUtils.isNotBlank(object.getCidade()) ||
            StringUtils.isNotBlank(object.getEstado()) ||
            StringUtils.isNotBlank(object.getEmail()) ||
            StringUtils.isNotBlank(object.getTelefone()) ||
            StringUtils.isNotBlank(object.getNomeInstituicao()) ||
            StringUtils.isNotBlank(object.getPais()));
    }
}