package br.com.mauda.seminario.cientificos.bc;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dao.InscricaoDAO;
import br.com.mauda.seminario.cientificos.dto.InscricaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Estudante;
import br.com.mauda.seminario.cientificos.model.Inscricao;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class InscricaoBC extends PatternCrudBC<Inscricao, InscricaoDTO, InscricaoDAO> {

    @Inject
    private SeminarioBC seminarioBC;
    @Inject
    private EstudanteBC estudanteBC;

    @Override
    protected void validateForDataModification(Inscricao object) {
        if (object == null) {
            throw new SeminariosCientificosException("ER0040");
        }

        if (object.getSituacao() == null) {
            throw new SeminariosCientificosException("ER0040");
        }

        if (object.getDireitoMaterial() == null) {
            throw new SeminariosCientificosException("ER0041");
        }

        this.seminarioBC.validateForDataModification(object.getSeminario());
        this.estudanteBC.validateForDataModification(object.getEstudante());

    }

    public void comprar(Inscricao inscricao, Estudante estudante, Boolean direitoMaterial) {
        this.estudanteBC.validateForDataModification(estudante);

        if (inscricao == null) {
            throw new SeminariosCientificosException("ER0040");
        }

        if (direitoMaterial == null) {
            throw new SeminariosCientificosException("ER0041");
        }

        if (!inscricao.getSituacao().equals(SituacaoInscricaoEnum.DISPONIVEL)) {
            throw new SeminariosCientificosException("ER0042");
        }

        inscricao.comprar(estudante, direitoMaterial);
        this.update(inscricao);

    }

    public void realizarCheckIn(Inscricao inscricao) {
        if (inscricao == null) {
            throw new SeminariosCientificosException("ER0040");
        }
        if (!inscricao.getSituacao().equals(SituacaoInscricaoEnum.COMPRADO)) {
            throw new SeminariosCientificosException("ER0043");
        }

        this.estudanteBC.validateForDataModification(inscricao.getEstudante());

        inscricao.realizarCheckIn();
        this.update(inscricao);

    }

    @Override
    protected boolean validateForFindData(InscricaoDTO object) {
        // TODO Auto-generated method stub
        return object != null && (object.getId() != null ||
            object.getDataSeminario() != null ||
            !object.getSituacoes().isEmpty() ||
            object.getDireitoMaterial() != null ||
            StringUtils.isNotBlank(object.getNomeEstudante()) ||
            StringUtils.isNotBlank(object.getTituloSeminario()));

    }
}