package br.com.mauda.seminario.cientificos.dao.util;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.hibernate.Transaction;

import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;

@Interceptor
@OperacaoTransacional
public class OperacaoTransacionalInterceptor {

    @AroundInvoke
    public Object manageSession(InvocationContext ic) throws Exception {
        if (!(ic.getTarget() instanceof PossuiSession)) {
            throw new SeminariosCientificosException("Na foi possível usar essa sessão");
        }

        Transaction tx = null;
        try {
            tx = ((PossuiSession) ic.getTarget()).getSession().beginTransaction();
            Object result = ic.proceed();
            tx.commit();
            return result;
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw new SeminariosCientificosException(ex);
        }
    }

}
