package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.InscricaoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Inscricao;
import br.com.mauda.seminario.cientificos.model.Professor;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class InscricaoDAO extends PatternCrudDAO<Inscricao, InscricaoDTO> {

    private static final long serialVersionUID = 1L;

    public InscricaoDAO() {
        super(Inscricao.class);
    }

    @Override
    public void inicializaLazyObjects(Inscricao object) {
        if (object != null) {

            if (object.getEstudante() != null) {
                Hibernate.initialize(object.getEstudante().getInscricoes());
            }

            Hibernate.initialize(object.getSeminario().getInscricoes());
            if (object.getSeminario().getProfessores() != null) {
                Hibernate.initialize(object.getSeminario().getProfessores());
                for (Professor professor : object.getSeminario().getProfessores()) {
                    Hibernate.initialize(professor.getSeminarios());
                }
            }
            Hibernate.initialize(object.getSeminario().getAreasCientificas());

        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<Inscricao> findByFilter(InscricaoDTO filter) {
        try {

            Criteria c = this.session.createCriteria(this.entityClassName);
            c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

            if (filter != null) {

                if (filter.getId() != null) {
                    c.add(Restrictions.eq("id", filter.getId()));
                }
                if (filter.getDireitoMaterial() != null) {
                    c.add(Restrictions.eq("direitoMaterial", filter.getDireitoMaterial()));
                }

                if (!filter.getSituacoes().isEmpty()) {

                    for (SituacaoInscricaoEnum sitaucao : filter.getSituacoes()) {
                        if (sitaucao.equals(SituacaoInscricaoEnum.COMPRADO)) {
                            c.add(Restrictions.eq("situacao", SituacaoInscricaoEnum.COMPRADO));
                            break;

                        }
                        if (sitaucao.equals(SituacaoInscricaoEnum.CHECKIN)) {
                            c.add(Restrictions.eq("situacao", SituacaoInscricaoEnum.CHECKIN));
                            break;

                        }
                        if (sitaucao.equals(SituacaoInscricaoEnum.DISPONIVEL)) {
                            c.add(Restrictions.eq("situacao", SituacaoInscricaoEnum.DISPONIVEL));
                            break;

                        }
                    }
                }

                if (StringUtils.isNotBlank(filter.getNomeEstudante())) {
                    Criteria cEstudante = c.createCriteria("estudante");
                    cEstudante.add(Restrictions.ilike("nome", "%" + filter.getNomeEstudante() + "%"));
                }

                if (StringUtils.isNotBlank(filter.getTituloSeminario()) || filter.getDataSeminario() != null) {
                    Criteria cSeminario = c.createCriteria("seminario");

                    if (StringUtils.isNotBlank(filter.getTituloSeminario())) {
                        cSeminario.add(Restrictions.ilike("titulo", "%" + filter.getTituloSeminario() + "%"));
                    }

                    if (filter.getDataSeminario() != null) {
                        cSeminario.add(Restrictions.eq("data", filter.getDataSeminario()));
                    }
                }
            }

            Collection<Inscricao> collection = c.list();
            for (Inscricao object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }

    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
