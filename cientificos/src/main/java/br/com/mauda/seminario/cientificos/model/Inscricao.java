package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

@Entity
@Table(name = "TB_INSCRICAO")
public class Inscricao implements IdentifierInterface, Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DIREITO_MATERIAL")
    private Boolean direitoMaterial;

    @Enumerated(EnumType.STRING)
    @Column(name = "SITUACAO", nullable = false)
    private SituacaoInscricaoEnum situacao;

    @ManyToOne
    @JoinColumn(name = "ID_SEMINARIO")
    private Seminario seminario;

    @ManyToOne
    @JoinColumn(name = "ID_ESTUDANTE")
    private Estudante estudante;

    public Inscricao() {

    }

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.getInscricoes().add(this);
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    public void comprar(Estudante estudante, boolean direitoMaterial) {
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
        this.estudante.getInscricoes().add(this);
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public void setSituacao(SituacaoInscricaoEnum situacao) {
        this.situacao = situacao;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public void setSeminario(Seminario seminario) {
        this.seminario = seminario;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}