package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.SeminarioDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Professor;
import br.com.mauda.seminario.cientificos.model.Seminario;

public class SeminarioDAO extends PatternCrudDAO<Seminario, SeminarioDTO> {

    private static final long serialVersionUID = 1L;

    public SeminarioDAO() {
        super(Seminario.class);
    }

    @Override
    public void inicializaLazyObjects(Seminario object) {
        if (object != null) {

            if (object.getProfessores() != null) {
                Hibernate.initialize(object.getProfessores());
                for (Professor professor : object.getProfessores()) {
                    Hibernate.initialize(professor.getSeminarios());
                }
            }
            Hibernate.initialize(object.getInscricoes());
            Hibernate.initialize(object.getAreasCientificas());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<Seminario> findByFilter(SeminarioDTO filter) {
        try {
            Criteria c = this.session.createCriteria(this.entityClassName);
            c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

            if (filter.getId() != null) {
                c.add(Restrictions.eq("id", filter.getId()));
            }

            if (filter.getData() != null) {
                c.add(Restrictions.eq("data", filter.getData()));
            }

            if (filter.getMesaRedonda() != null) {
                c.add(Restrictions.eq("mesaRedonda", filter.getMesaRedonda()));
            }

            if (StringUtils.isNotBlank(filter.getDescricao())) {
                c.add(Restrictions.ilike("descricao", "%" + filter.getDescricao() + "%"));
            }

            if (StringUtils.isNotBlank(filter.getTitulo())) {
                c.add(Restrictions.ilike("titulo", "%" + filter.getTitulo() + "%"));
            }

            if (StringUtils.isNotBlank(filter.getNomeAreaCientifica())) {

                c.createAlias("areasCientificas", "areasCientificas");
                c.add(Restrictions.ilike("areasCientificas.nome", "%" + filter.getNomeAreaCientifica() + "%"));

            }

            if (StringUtils.isNotBlank(filter.getNomeProfessor())) {
                // Criteria cProfessor = c.createCriteria("professores");
                c.createAlias("professores", "professores");
                c.add(Restrictions.ilike("professores.nome", "%" + filter.getNomeProfessor() + "%"));
            }

            Collection<Seminario> collection = c.list();
            for (Seminario object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }

    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
