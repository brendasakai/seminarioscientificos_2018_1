package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.CursoDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Curso;

public class CursoDAO extends PatternCrudDAO<Curso, CursoDTO> {

    private static final long serialVersionUID = 1L;

    public CursoDAO() {
        super(Curso.class);
    }

    @Override
    public void inicializaLazyObjects(Curso object) {
        if (object != null) {
            Hibernate.initialize(object.getAreaCientifica().getCursos());
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<Curso> findByFilter(CursoDTO filter) {
        try {

            Criteria c = this.session.createCriteria(this.entityClassName);
            c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

            if (filter.getId() != null) {
                c.add(Restrictions.eq("id", filter.getId()));
            }

            if (StringUtils.isNotBlank(filter.getNome())) {
                c.add(Restrictions.ilike("nome", "%" + filter.getNome() + "%"));
            }

            if (StringUtils.isNotBlank(filter.getNomeAreaCientifica())) {
                Criteria cAreaCientifica = c.createCriteria("areaCientifica");

                if (StringUtils.isNotBlank(filter.getNomeAreaCientifica())) {
                    cAreaCientifica.add(Restrictions.ilike("nome", "%" + filter.getNomeAreaCientifica() + "%"));
                }
            }

            Collection<Curso> collection = c.list();
            for (Curso object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;

        } catch (Exception e) {
            throw new SeminariosCientificosException(e);
        }
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
