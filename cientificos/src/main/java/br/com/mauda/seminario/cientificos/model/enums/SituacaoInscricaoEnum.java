package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1, "Disponivel"),
    COMPRADO(2, "Comprado"),
    CHECKIN(3, "Checkin");

    private final int id;
    private final String nome;

    SituacaoInscricaoEnum(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public static SituacaoInscricaoEnum getBy(String nome) {
        switch (nome) {
            case "DISPONIVEL":
                return DISPONIVEL;
            case "COMPRADO":
                return COMPRADO;
            case "CHECKIN":
                return CHECKIN;
            default:
                return null;
        }
    }

    public int getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

}
