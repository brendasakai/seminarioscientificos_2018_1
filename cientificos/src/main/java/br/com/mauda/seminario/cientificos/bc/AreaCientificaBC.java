package br.com.mauda.seminario.cientificos.bc;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.dao.AreaCientificaDAO;
import br.com.mauda.seminario.cientificos.dto.AreaCientificaDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.AreaCientifica;

public class AreaCientificaBC extends PatternCrudBC<AreaCientifica, AreaCientificaDTO, AreaCientificaDAO> {

    @Override
    protected void validateForDataModification(AreaCientifica object) {

        if (object == null) {
            throw new SeminariosCientificosException("ER0003");
        }

        if (StringUtils.isBlank(object.getNome())) {
            throw new SeminariosCientificosException("ER0010");
        }

        if (object.getNome().length() > 50) {
            throw new SeminariosCientificosException("ER0010");
        }

    }

    @Override
    protected boolean validateForFindData(AreaCientificaDTO object) {
        return object != null && (object.getId() != null || StringUtils.isNotBlank(object.getNome()));
    }
}
