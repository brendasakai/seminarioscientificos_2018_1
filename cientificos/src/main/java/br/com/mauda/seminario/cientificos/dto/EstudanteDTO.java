package br.com.mauda.seminario.cientificos.dto;

public class EstudanteDTO {

    private Long id;
    private String cidade;
    private String email;
    private String estado;
    private String nome;
    private String pais;
    private String nomeInstituicao;
    private String cidadeInstituicao;
    private String estadoInstituicao;
    private String paisInstituicao;
    private Long idInstituicao;

    private String telefone;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCidade() {
        return this.cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeInstituicao() {
        return this.nomeInstituicao;
    }

    public void setNomeInstituicao(String nomeInstituicao) {
        this.nomeInstituicao = nomeInstituicao;
    }

    public String getPais() {
        return this.pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCidadeInstituicao() {
        return this.cidadeInstituicao;
    }

    public void setCidadeInstituicao(String cidadeInstituicao) {
        this.cidadeInstituicao = cidadeInstituicao;
    }

    public String getEstadoInstituicao() {
        return this.estadoInstituicao;
    }

    public void setEstadoInstituicao(String estadoInstituicao) {
        this.estadoInstituicao = estadoInstituicao;
    }

    public String getPaisInstituicao() {
        return this.paisInstituicao;
    }

    public void setPaisInstituicao(String paisInstituicao) {
        this.paisInstituicao = paisInstituicao;
    }

    public Long getIdInstituicao() {
        return this.idInstituicao;
    }

    public void setIdInstituicao(Long idInstituicao) {
        this.idInstituicao = idInstituicao;
    }

}
