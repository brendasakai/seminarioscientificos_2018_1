package br.com.mauda.seminario.cientificos.dao;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import br.com.mauda.seminario.cientificos.dto.AreaCientificaDTO;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.AreaCientifica;

public class AreaCientificaDAO extends PatternCrudDAO<AreaCientifica, AreaCientificaDTO> {

    private static final long serialVersionUID = 1L;

    public AreaCientificaDAO() {
        super(AreaCientifica.class);
    }

    @Override
    public void inicializaLazyObjects(AreaCientifica object) {
        if (object != null) {
            Hibernate.initialize(object.getCursos());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<AreaCientifica> findByFilter(AreaCientificaDTO filter) {
        try {
            Criteria c = this.session.createCriteria(this.entityClassName);
            c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

            if (filter.getId() != null) {
                c.add(Restrictions.eq("id", filter.getId()));
            }

            if (StringUtils.isNotBlank(filter.getNome())) {
                c.add(Restrictions.like("nome", "%" + filter.getNome() + "%"));
            }

            Collection<AreaCientifica> collection = c.list();
            for (AreaCientifica object : collection) {
                this.inicializaLazyObjects(object);
            }
            return collection;

        } catch (Exception e) {
            throw new SeminariosCientificosException("ER0003");
        }
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
