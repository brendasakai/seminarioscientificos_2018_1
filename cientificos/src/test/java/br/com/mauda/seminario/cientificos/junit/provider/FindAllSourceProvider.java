package br.com.mauda.seminario.cientificos.junit.provider;

import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.support.AnnotationConsumer;
import org.junit.platform.commons.util.Preconditions;

import br.com.mauda.seminario.cientificos.bc.PatternCrudBC;
import br.com.mauda.seminario.cientificos.util.WeldContext;

public class FindAllSourceProvider implements ArgumentsProvider, AnnotationConsumer<FindAllSource> {

    protected PatternCrudBC<?, ?, ?> bc;

    @Override
    public void accept(FindAllSource source) {
        this.bc = WeldContext.getInstanciatedClass(source.value());
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        Collection<?> objs = this.bc.findAll();
        Preconditions.notNull(objs, "O retorno do metodo findAll nao pode ser nulo");
        Preconditions.notEmpty(objs, "O retorno do metodo findAll deve conter algum elemento");
        return objs.stream().map(Arguments::of);
    }
}
